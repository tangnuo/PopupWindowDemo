package com.kedacom.popupwindowdemo

import android.content.Context
import android.graphics.Point
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Button
import android.widget.PopupWindow
import android.widget.Toast


/**
 * PopupWindow(悬浮框)的基本使用
 * 需求：
 * 1、悬浮窗显示在按钮下方；
 * 2、悬浮窗的背景色不遮挡标题栏；
 * 3、悬浮窗的高度撑满剩余控件（计算出高度）；
 *
 *
 * https://www.runoob.com/w3cnote/android-tutorial-popupwindow.html
 *
 *
 * https://www.jcodecraeer.com/a/anzhuokaifa/androidkaifa/2014/0702/1627.html
 */
class MainActivity : AppCompatActivity() {
    private var mContext: Context? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mContext = this@MainActivity
        val btn_show = findViewById<Button>(R.id.btn_show)
        btn_show.setOnClickListener { v: View ->
            initPopWindow(
                v
            )
        }
    }

    private fun initPopWindow(targetView: View) {
        val view: View = LayoutInflater.from(mContext).inflate(R.layout.layout_popup, null, false)
        //1.构造一个PopupWindow，参数依次是加载的View，宽高
        val popWindow = PopupWindow(
            view,
            ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true
        )
        popWindow.height = getPopHeight(targetView)
        popWindow.animationStyle = R.anim.anim_pop //设置加载动画

        //这些为了点击非PopupWindow区域，PopupWindow会消失的，如果没有下面的
        //代码的话，你会发现，当你把PopupWindow显示出来了，无论你按多少次后退键
        //PopupWindow并不会关闭，而且退不出程序，加上下述代码可以解决这个问题
        popWindow.isTouchable = true
        popWindow.isOutsideTouchable = true
        popWindow.setBackgroundDrawable(ColorDrawable(0x00000000)) //要为popWindow设置一个背景才有效

        //设置popupWindow显示的位置，参数依次是参照View，x轴的偏移量，y轴的偏移量
        popWindow.showAsDropDown(targetView, 0, 0)
        val btn_xixi = view.findViewById<Button>(R.id.btn_xixi)
        val btn_hehe = view.findViewById<Button>(R.id.btn_hehe)
        val bgView = view.findViewById<View>(R.id.bg_view)

        //设置popupWindow里的按钮的事件
        btn_xixi.setOnClickListener { v12: View? ->
            Toast.makeText(
                this@MainActivity,
                "你点击了嘻嘻~",
                Toast.LENGTH_SHORT
            ).show()
        }
        btn_hehe.setOnClickListener { v13: View? ->
            Toast.makeText(this@MainActivity, "你点击了呵呵~", Toast.LENGTH_SHORT).show()
            popWindow.dismiss()
        }
        //点击空白的区域，关闭悬浮窗
        bgView.setOnClickListener { v: View? -> popWindow.dismiss() }
    }

    private fun getPopHeight(targetView: View): Int {
        val screenHeight = getScreenHigh(this) //屏幕高度
        val statusBatHeight = statusBarHeight //状态栏高度
        val top = targetView.top
        val viewHeight = targetView.height
        Log.d(
            "caowj",
            "屏幕高度：$screenHeight,状态栏高度：$statusBatHeight,按钮坐标：$top，按钮高度：$viewHeight"
        )
        val popHeight = screenHeight - statusBatHeight - top - viewHeight
        Log.d("caowj", "得到弹窗的高度：$popHeight")
        return popHeight
    }

    /**
     * 获取状态栏高度
     */
    private val statusBarHeight: Int
        private get() {
            val resources = this.resources
            val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
            return resources.getDimensionPixelSize(resourceId)
        }

    /**
     * 获取屏幕高度
     */
    private fun getScreenHigh(context: Context): Int {
        val windowManager = context.getSystemService(WINDOW_SERVICE) as WindowManager
        val display = windowManager.defaultDisplay
        val outPoint = Point()
        if (Build.VERSION.SDK_INT >= 19) {
            // 可能有虚拟按键的情况
            display.getRealSize(outPoint)
        } else {
            // 不可能有虚拟按键
            display.getSize(outPoint)
        }
        val mRealSizeHeight: Int //手机屏幕真实高度
        mRealSizeHeight = outPoint.y
        return mRealSizeHeight
    }
}