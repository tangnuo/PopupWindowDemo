# PopupWindowDemo

### 介绍

 * 1、悬浮窗显示在按钮下方；
 * 2、悬浮窗的背景色不遮挡标题栏；
 * 3、悬浮窗的高度撑满剩余控件（计算出高度）；
## 效果图
![输入图片说明](https://images.gitee.com/uploads/images/2022/0415/144144_f9da2cc6_402106.png "屏幕截图.png")


